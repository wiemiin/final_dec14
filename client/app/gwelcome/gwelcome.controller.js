(function () {
    angular
        .module('paApp')
        .controller('GwelcomeCtrl', GwelcomeCtrl)

    GwelcomeCtrl.$inject = ["paAppAPI", "$state", "$scope", "$filter", "$window", '$q', 'user'];

    function GwelcomeCtrl(paAppAPI, $state, $scope, $filter, $window, $q, user) {
        var self = this;

        self.user = user;

        self.cal = {};
        self.gcal = [];
        self.gclient = {};
        self.gclients = [];
        self.current = {};
        self.message = "";
        self.msg = '';

        // self.cal.notification = "24 hours";
        // self.cal.training = "work out";
        // self.cal.mobile = "No mobile listed";

        self.todaydate = new Date();

        //// GOOGLE CALENDAR

        self.searchAllGcal = function () {

            console.log(user.google.email)

            $q.all([
                paAppAPI.searchAllGcal(user.google.email),
                paAppAPI.searchAllGclient(user._id)
            ]).then(function (results) {

                console.log(results[0].length)
                self.gcal = results[0];

                console.log(results[1].length)
                self.gclients = results[1]

                for (var i = 0; i < self.gcal.length; i++) {
                    self.gcal[i].datetime = (self.gcal[i].datetime) ? new Date(self.gcal[i].datetime) : new Date();
                    // self.gcal[i].notification = "24 hours"
                    // self.gcal[i].training = "work out"
                    // self.gcal[i].mobile = "No mobile listed"
                    //console.log("Summary " + self.gcal[i].summary) 

                    if (self.gcal[i].attendees && self.gcal[i].attendees.length && self.gcal[i].attendees[0].length) {
                        self.gcal[i].attendee = self.gcal[i].attendees[0][0].email
                    }
                }
                //results[0] = from google
                //results[1] = from client

                for (var x = 0; x < results[0].length; x++) {
                    for (var y = 0; y < results[1].length; y++) {
                        var a = results[0][x].summary;
                        var b = results[1][y].name;
                        //console.log(a)
                        //console.log(b)

                        if (a.trim().toLowerCase() == b.trim().toLowerCase()) {
                            results[0][x].mobile = results[1][y].mobile
                        }
                    }
                }

            }).catch(function (error) {

                console.log(error);
                if (error.status == 404) {
                    self.message = "No events found in calendar";
                }

            })

        }

        self.searchAllGcal();

        self.addGclient = function () {
            self.showSuccessMessage = false;
            self.showFailureMessage = false;

            console.log("Adding Google client... ");
            console.log(self.gclient.name);
            console.log(self.gclient.mobile);
            //console.log(self.user.google);
            console.log(self.user._id);

            self.gclient.userid = self.user._id;

            paAppAPI.addGclient(self.gclient)
                .then(function (result) {
                    console.log(result);
                    self.message = result.data;
                    self.showSuccessMessage = true;
                    self.gclients.unshift({
                        'name': self.gclient.name,
                        'mobile': self.gclient.mobile
                    });

                    if (self.addGclientForm) {
                        self.addGclientForm.$setPristine();
                        self.addGclientForm.$setUntouched();
                        self.gclient = {};
                    }

                }).catch(function (err) {
                    console.log(err);
                    self.message = err.data;
                    self.msg = 'CLIENT ALREADY EXIST';
                    self.showFailureMessage = true;
                });
        };

        self.searchAllGclient = function () {

            paAppAPI.searchAllGclient(user._id)
                .then(function (result) {
                    console.log(result);

                    self.gclients = result;

                }).catch(function (err) {
                    console.log(err);
                    if (err.status == 404) {
                        self.message = "No clients found";

                    }
                });

        }

        //SAVE Google Client 

        self.saveGclient = function (gclient) {

            console.log("Saving Google client... ");

            paAppAPI.updateGclient(gclient)
                .then(function (result) {
                    //console.log("Gclient result " + JSON.stringify(result));

                    self.message = result.data;
                    self.showSuccessMessage = true;

                }).catch(function (err) {
                    console.log(err);
                    self.message = err.data;
                    self.showFailureMessage = true;
                });

        }

        self.deleteGclient = function (id) {
            console.log("Deleting Google client... ");

            paAppAPI.deleteGclient(id)
                .then(function (result) {
                    console.log(id);
                    console.log(result);
                    self.message = result;

                }).catch(function (err) {
                    console.log(err);
                });
        };

        self.removeRow = function (gclientIndex) {
            self.gclients.splice(gclientIndex, 1);
        };

        //SAVE Google Calendar 

        self.saveGcal = function (cal) {

            console.log("Saving Google client... ");

            paAppAPI.updateGcal(cal)
                .then(function (result) {
                    //console.log("Saving Google client " + JSON.stringify(result));
                    console.log("Saving Google name " + result.config.data.cal.summary);
                    console.log("Saving Google mobile " + result.config.data.cal.mobile);

                    self.message = result.data;
                    self.showSuccessMessage = true;

                }).catch(function (err) {
                    console.log(err);
                    self.message = err.data;
                    self.showFailureMessage = true;
                });

        }

        self.deleteGcal = function (cal) {
            console.log("Deleting Google client... ");

            paAppAPI.deleteGcal(cal)
                .then(function (result) {
                    console.log(cal);
                    console.log(result);
                    self.message = result;

                }).catch(function (err) {
                    console.log(err);
                });
        };

        self.removeRow = function (calIndex) {
            self.gcal.splice(calIndex, 1);
        };



    }

})();