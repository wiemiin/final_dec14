(function () {
  angular
    .module('paApp')
    .controller('WelcomeCtrl', WelcomeCtrl)

  WelcomeCtrl.$inject = ["paAppAPI", "$state", "$scope", "$filter", "$window", 'user', "$rootScope"];

  function WelcomeCtrl(paAppAPI, $state, $scope, $filter, $window, user, $rootScope) {
    var self = this;

    self.user = user;
    self.client = {};
    self.clients = [];
    self.booking = {};
    self.bookings = [];
    // self.cal = {};
    // self.gcal = [];
    self.current = {};
    self.message = "";
    self.msg = '';

    self.radio = "client";

    self.todaydate = new Date();

    if (!self.booking.datetime) {
      self.booking.datetime = new Date($filter('date')(new Date(), 'yyyy-MM-ddTHH:mm'));
    }

    self.showResult = false;

    //UIB TYPEAHEAD TO FETCH CLIENTS WHEN APPOINTMENT RADIO BUTTON IS SELECTED
    self.reloadClient = function () {
      console.log(user._id);

      paAppAPI.searchAllClients(user._id)
        .then(function (result) {
          console.log("here");
          console.log(result);

          self.clients = result;

          console.log(self.clients)

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No clients found";

          }
        });
    }

    // $scope.reloadAdd = function () {
    //   $window.location.reload();
    //   $rootScope.loggedinuser = true;
    // }

    self.searchClientName = function () {

      paAppAPI.searchClientName(self.term, user._id)
        .then(function (result) {
          console.log(result);
          if (result.length === 0) {
            self.msg = 'CLIENT DOES NOT EXIST';
          }
          console.log(result);
          self.clients = result;

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No clients found";

          }
        });

    };

    self.searchAllClients = function () {

      self.showResult = false;
      self.showClient = true;

      paAppAPI.searchAllClients(user._id)
        .then(function (result) {
          console.log(result);

          self.clients = result;

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No clients found";

          }
        });

    }


    // $scope.reloadRoute = function () {
    //   console.log("Reloading...");
    //   $window.location.reload();
    // }

    self.addClient = function () {
      self.showSuccessMessage = false;
      self.showFailureMessage = false;

      console.log("Adding client... ");
      console.log(self.client.firstname);
      console.log(self.client.lastname);
      console.log(self.client.mobile);
      console.log(self.user.local);
      console.log(self.user._id);

      self.client.userid = self.user._id;

      paAppAPI.addClient(self.client)
        .then(function (result) {
          console.log(result);
          self.message = result.data;
          self.showSuccessMessage = true;
          self.clients.unshift({
            'firstname': self.client.firstname,
            'lastname': self.client.lastname,
            'mobile': self.client.mobile,
            'userid': self.client.userid
          });

          if (self.addClientForm) {
            self.addClientForm.$setPristine();
            self.addClientForm.$setUntouched();
            self.client = {};
          }

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.msg = 'CLIENT ALREADY EXIST';
          self.showFailureMessage = true;
        });
    };

    self.editClient = function (id) {

      console.log("Editing client... ");

      paAppAPI.getClientById(id)
        .then(function (result) {
          console.log(id);
          console.log(result);
          self.message = result;
        }).catch(function (err) {
          console.log(err)
        });

    }

    self.saveClient = function (client) {

      console.log("Saving client... ");

      paAppAPI.updateClient(client)
        .then(function (result) {
          console.log(result);

          self.message = result.data;
          self.showSuccessMessage = true;

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.showFailureMessage = true;
        });

    }

    self.deleteClient = function (id) {
      console.log("Deleting client... ");

      paAppAPI.deleteClient(id)
        .then(function (result) {
          console.log(id);
          console.log(result);
          self.message = result;

        }).catch(function (err) {
          console.log(err);
        });
    };

    self.removeRow = function (clientIndex) {
      self.clients.splice(clientIndex, 1);
    };

     //// GOOGLE CALENDAR

    //  self.searchAllGcal = function () {
      
    //         console.log(user.google.email)

    //         paAppAPI.searchAllGcal(user.google.email)
    //           .then(function (result) {
    //             console.log(result);
    //             self.gcal = result;
                
      
    //             for (var i = 0; i < self.gcal.length; i++) {
    //               self.gcal[i].datetime = (self.gcal[i].datetime) ? new Date(self.gcal[i].datetime) : new Date();
    //               self.gcal[i].notification = "24 hours"
    //               self.gcal[i].training = "work out"
    //               self.gcal[i].mobile = "No mobile listed"

    //               if(self.gcal[i].attendees && self.gcal[i].attendees.length && self.gcal[i].attendees[0].length) {
    //                 self.gcal[i].attendee = self.gcal[i].attendees[0][0].email
    //               }

    //               //console.log("Attendee " + self.gcal[i].attendees[0])
    //               //console.log("Attendee " + JSON.stringify(self.gcal[i].attendees[0][0]))
    //             }
      
    //           }).catch(function (err) {
    //             console.log(err);
    //             if (err.status == 404) {
    //               self.message = "No events found in calendar";
    //             }
    //           });
    //       }

    //// BOOKING

    self.searchAllBookings = function () {

      console.log(user._id)

      paAppAPI.searchAllBookings(user._id)
        .then(function (result) {
          console.log(result);
          self.bookings = result;

          for (var i = 0; i < self.bookings.length; i++) {
            self.bookings[i].datetime = (self.bookings[i].datetime) ? new Date(self.bookings[i].datetime) : new Date();
          }

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No bookings found";
          }
        });
    }

    self.searchBookingByName = function () {

      paAppAPI.searchBookingByName(self.term, user._id)
        .then(function (result) {
          if (result.length === 0) {
            self.msg = 'NO BOOKING WITH THIS CLIENT EXIST';
          }
          console.log(result);
          self.bookings = result;

          for (var i = 0; i < self.bookings.length; i++) {
            self.bookings[i].datetime = (self.bookings[i].datetime) ? new Date(self.bookings[i].datetime) : new Date();
          }

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No bookings found";
          }
        });

    };

    self.addBooking = function () {

      console.log("Adding booking... ");
      console.log(self.booking.training);
      console.log(self.booking.client.firstname);
      console.log(self.booking.client.lastname);
      console.log(self.booking.datetime);
      console.log(self.booking.notification);
      console.log("Client id is " + self.booking.client._id);

      self.showResult = true;

      self.booking.clientid = self.booking.client._id;
      self.booking.userid = self.user._id;
      self.booking.user = user;

      paAppAPI.addBooking(self.booking)
        .then(function (result) {
          console.log(result);
          self.message = result.data;
          self.showSuccessMessage = true;
          self.bookings.unshift({
            'training': self.booking.training,
            'firstname': self.booking.client.firstname,
            'lastname': self.booking.client.lastname,
            'datetime': self.booking.datetime,
            'notification': self.booking.notification,
            'clientid': self.booking.clientid,
            'userid': self.booking.userid
          });

          if (self.addBookingForm) {
            self.addBookingForm.$setPristine();
            self.addBookingForm.$setUntouched();
            self.booking = {};
          }

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.showFailureMessage = true;
        });
    }

    self.saveBooking = function (booking) {

      console.log("Saving booking... ");

      paAppAPI.updateBooking(booking)
        .then(function (result) {
          console.log(result);
          self.message = result.data;
          self.showSuccessMessage = true;

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.showFailureMessage = true;
        });

    }

    self.deleteBooking = function (id) {
      console.log("Deleting booking... ");

      paAppAPI.deleteBooking(id)
        .then(function (result) {
          console.log(id);
          console.log(result);
          self.message = result;

        }).catch(function (err) {
          console.log(err);
        });
    };

    self.removeRow = function (bookingIndex) {
      self.clients.splice(bookingIndex, 1);
    };

    //GOOGLE CALENDAR INIT
    //self.searchAllGcal();

    // if (self.user.google = true) {
    //   self.searchAllGcal();
    // }

  }

  

})();