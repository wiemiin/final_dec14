(function () {
    angular
        .module("paApp")
        .service("paAppAPI", [
            '$http',
            "$q",
            paAppAPI
        ]);

    function paAppAPI($http, $q) {
        var self = this;

        //USER

        self.addUser = function (user) {
            return $http.post("/api/user", user);
        };

         //CLIENT

        self.addClient = function (client) {
            return $http.post("/api/client", client);
        };

        self.updateClient = function (client) {
            var defer = $q.defer();
            const id = client._id; //IMPORTANT!

            $http.put("/api/client/" + id, {
                    client: client
                })
                .then(function (result) {
                    //alert(result.data.msg);
                    console.log(client);
                    defer.resolve(result);

                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;

        }

        self.deleteClient = function (id) {
            var defer = $q.defer();

            $http.delete("/api/client/" + id).then(function (result) {
                //alert(result.data.msg);
                console.log(result);
                defer.resolve(result);

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;

        };

        self.searchClientName = function (firstname, id) {
            var defer = $q.defer();

            $http.get(`/api/client?keyword=${firstname}&filterby=${id}`).then(function (result) {
                console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        self.searchAllClients = function (id) {
            var defer = $q.defer();

            $http.get("/client/?id=" + id).then(function (result) {
                console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        //BOOKING
        
        self.addBooking = function (booking) {
            return $http.post("/api/booking", booking);
        };

        self.updateBooking = function (booking) {
            var defer = $q.defer();
            const id = booking._id; //IMPORTANT!

            $http.put("/api/booking/" + id, {
                    booking: booking
                })
                .then(function (result) {
                    //alert(result.data.msg);
                    console.log(booking);
                    defer.resolve(result);

                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;

        }

        self.deleteBooking = function (id) {
            var defer = $q.defer();

            $http.delete("/api/booking/" + id).then(function (result) {
                //alert(result.data.msg);
                console.log(result);
                defer.resolve(result);

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;

        };

        self.searchAllBookings = function (id) {
            var defer = $q.defer();

            $http.get("/booking?id=" + id).then(function (result) {
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        self.searchBookingByName = function (firstname, id) {
            var defer = $q.defer();

            $http.get(`/api/booking?keyword=${firstname}&filterby=${id}`).then(function (result) {
                console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        //GCAL 

        self.searchAllGcal = function (email) {
            var defer = $q.defer();

            $http.get("/api/gcal?email=" + email).then(function (result) {
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        self.updateGcal = function (cal) {
            var defer = $q.defer();
            const id = cal._id; //IMPORTANT!

            $http.put("/api/gcal/" + id, {
                    cal: cal
                })
                .then(function (result) {
                    //alert(result.data.msg);
                    console.log(cal);
                    defer.resolve(result);

                }).catch(function (err) {
                    defer.reject(err);
                });

                const gcalId = cal.gcalId; 

                $http.patch("/api/gcalupdate/" + gcalId, {
                    cal: cal
                })
                .then(function (result) {
                    //alert(result.data.msg);
                    console.log(cal);
                    defer.resolve(result);
    
                }).catch(function (err) {
                    defer.reject(err);
                });
    
            return defer.promise;
           
        }

        self.deleteGcal = function (cal) {
            
            var defer = $q.defer();
            const id = cal._id;

            $http.delete("/api/gcal/" + id).then(function (result) {
                //alert(result.data.msg);
                console.log(result);
                defer.resolve(result);

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            const gcalId = cal.gcalId; 

            $http.delete("/api/gcaldelete/" + gcalId, {
                cal: cal
            })
            .then(function (result) {
                //alert(result.data.msg);
                console.log(cal);
                defer.resolve(result);

            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        //GCLIENT

        self.addGclient = function (gclient) {
            return $http.post("/api/gclient", gclient);
        };

        self.updateGclient = function (gclient) {
            var defer = $q.defer();
            const id = gclient._id; //IMPORTANT!

            $http.put("/api/gclient/" + id, {
                    gclient: gclient
                })
                .then(function (result) {
                    //alert(result.data.msg);
                    console.log(gclient);
                    defer.resolve(result);

                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;

        }

        self.searchAllGclient = function (id) {
            var defer = $q.defer();

            $http.get("/gclient/?id=" + id).then(function (result) {
                //console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        self.deleteGclient = function (id) {
            var defer = $q.defer();

            $http.delete("/api/gclient/" + id).then(function (result) {
                //alert(result.data.msg);
                console.log(result);
                defer.resolve(result);

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;

        };





    }

})();