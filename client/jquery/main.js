$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('a').mouseover();
});

var menuItems = $('.cssmenu li');

menuItems.on("click", function(event) {
    
  menuItems.removeClass("active");
  
  $(this).addClass("active");
  
  $(".cssmenu").css({
    "background": $(this).data("bg-color")
  });
  
  event.preventDefault();
});


// $(document).ready(function(){       
//     var scroll_start = 0;
//     var startchange = $('#startchange');
//     var offset = startchange.offset();
//      if (startchange.length){
//     $(document).scroll(function() { 
//        scroll_start = $(this).scrollTop();
//        if(scroll_start > offset.top) {
//            $(".navbar").css('background-color', 'rgba(0, 0, 0, 0.3)');
//         } else {
//            $('.navbar').css('background-color', 'transparent');
   
//         }
//     });
//      }
//  });
 