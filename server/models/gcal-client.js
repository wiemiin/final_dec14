var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var gcalclientSchema = new Schema({

	id: String,
	useremail: String,
	name: String,
	mobile: String,
	clientemail: String,
	userid: {type: Schema.Types.ObjectId, ref: 'User'}
	
});

module.exports = mongoose.model('GcalClient', gcalclientSchema);