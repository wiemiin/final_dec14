var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gcalbookingSchema = new Schema({
    
    gcalId: String,
    training: {type: String, default: "work out" },
    summary: String,
	email: String,
	mobile: String,
    datetime: Date,
    notification: {type: String, default: "24 hours" },
    attendees:   [[]]
});


module.exports = mongoose.model('GcalBooking', gcalbookingSchema);