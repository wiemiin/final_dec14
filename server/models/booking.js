var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bookingSchema = new Schema({
    
    id: String,
    training: String,
	firstname: String,
	lastname: String,
    datetime: Date,
    notification: String,
    clientid: {type: Schema.Types.ObjectId, ref: 'Client'},
    userid: {type: Schema.Types.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('Booking', bookingSchema);