//import and inform passport to use local strategy
var LocalStrategy = require('passport-local').Strategy;
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
var User = require('./models/user');
var GcalBooking = require('./models/gcal-event');
var GClient = require('./models/gcal-client');
var bCrypt = require('bcrypt-nodejs');
var config = require('./config');
var gcal = require('google-calendar');
const googleapi = require('googleapis');
const OAuth2 = googleapi.auth.OAuth2;
var schedule = require('node-schedule');



module.exports = function (app, passport) {

    passport.use(new LocalStrategy({ // redefine the field names the stratgey (passport-local) expects
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, email, password, done) {

        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function () {

            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({
                'local.email': email
            }, function (err, user) {
                // if there are any errors, return the error
                console.log("This is from auth.js " + user);
                console.log("This is from auth.js " + email);
                console.log("This is from auth.js " + password);
                // In case of any error, return using the done method
                if (err)
                    return done(err);
                // Username does not exist, log the error and redirect back
                if (!user) {
                    console.log(email + ' does not exists');
                    return done(null, false);
                } else {
                    if (!validPassword(user, password)) {
                        console.log('Invalid Password');
                        return done(null, false); // redirect back to login page
                    } //return done(null, false, req.flash('message', 'Invalid Password'));    
                } //return done(null, false, req.flash('message', 'User Not found.'));     
                // User exists but wrong password, log the error 
                // User and password both match, return user from done method
                // which will be treated like success
                return done(null, user);

            });

        });

    }));

    passport.use(new GoogleStrategy({
        clientID: config.Google_key,
        clientSecret: config.Google_secret,
        callbackURL: config.Google_callback_url
    }, function (token, refreshToken, profile, done) {

        //console.log(profile);

        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function () {
            console.log(token);

            // try to find the user based on their google id
            // email is profile.emails[0].value
            User.findOne({
                'google.id': profile.id
            }, function (err, user) {
                if (err)
                    return done(err);

                if (user) {
                    user.google.token = token;
                    user.google.refreshToken = refreshToken;
                    console.log(refreshToken)

                    let calendarId = profile.emails[0].value;

                    getGooglePeople(calendarId, token, refreshToken).
                    then(function (result) {

                        console.log(">>> == ", result.length);

                        for (let x = 0; x < result.length; x++) {
                            console.log(result[x].names[0].displayName)
                            console.log(result[x].phoneNumbers[0].value)
                            console.log(result[x].emailAddresses[0].value)

                            GClient.findOne({
                                'name': result[x].names[0].displayName
                            }, function (err, gClientData) {
                                if (gClientData == null) {
                                    var newGclient = new GClient();
                                    saveGclienttoMongodb(newGclient, result[x], calendarId, false)
                                } else {
                                    saveGclienttoMongodb(gClientData, result[x], calendarId, true);

                                }

                            });
                        }

                    }).catch(function (error) {
                        console.error(">> error = ", error);
                        //NEED AN ERROR MESSAGE FLAG
                    })


                    getGoogleCalendarEvents(calendarId, token, function (result) {
                        console.log(result.items.length);
                        for (let x = 0; x < result.items.length; x++) {

                            GcalBooking.findOne({
                                'gcalId': result.items[x].id
                            }, function (err, gCalbooking) {
                                if (gCalbooking == null) {
                                    var newGcal = new GcalBooking();

                                    var now = new Date().valueOf();
                                    var calDate = new Date(result.items[x].start.dateTime).valueOf()

                                    if (calDate > now) {
                                        saveGcaltoMongodb(newGcal, result.items[x], calendarId, false);
                                    }

                                } else {
                                    
                                    // GcalBooking.findOneAndUpdate({
                                    //     'gcalId': result.items[x].id
                                    // }, {
                                    //     $set: {
                                    //         "summary": result.items[x].summary,
                                    //         "datetime": result.items[x].start.dateTime
                                    //     }
                                    // }, function (err, gCalbooking) {

                                        var now = new Date().valueOf();
                                        var calDate = new Date(result.items[x].start.dateTime).valueOf()

                                        if (calDate > now) {
                                            saveGcaltoMongodb(gCalbooking, result.items[x], calendarId, true);
                                        }

                                    // })
                                }
                            });
                        };
                    });

                    user.save(function (err) {
                        if (err)
                            throw err;

                        return done(null, user);
                    });

                    // if a user is found, log them in


                } else {
                    // if the user isnt in our database, create a new user
                    var newUser = new User();

                    // set all of the relevant information
                    newUser.google.id = profile.id;
                    newUser.google.token = token;
                    newUser.google.refreshToken = refreshToken;
                    newUser.google.name = profile.displayName;
                    newUser.google.email = profile.emails[0].value; // pull the first email

                    let calendarId = profile.emails[0].value;
                    getGoogleCalendarEvents(calendarId, token, function (result) {
                        console.log(result.items.length);
                        for (let x = 0; x < result.items.length; x++) {

                            var newGcal = new GcalBooking();
                            var now = new Date().valueOf();

                            if (result.items[x] !== null) {

                                var calDate = new Date(result.items[x].start.dateTime).valueOf()

                                if (calDate > now) {
                                    saveGcaltoMongodb(newGcal, result.items[x], calendarId, false);
                                }
                            }
                        };
                    });

                    getGooglePeople(calendarId, token, refreshToken).
                    then(function (result) {
                        console.log(">>> == ", result.length);

                        for (let x = 0; x < result.length; x++) {
                            console.log(result[x].names[0].displayName)
                            console.log(result[x].phoneNumbers[0].value)
                            console.log(result[x].emailAddresses[0].value)

                            GClient.findOne({
                                'name': result[x].names[0].displayName
                            }, function (err, gClientData) {
                                if (gClientData == null) {
                                    var newGclient = new GClient();
                                    saveGclienttoMongodb(newGclient, result[x], calendarId, false)
                                } else {
                                    saveGclienttoMongodb(gClientData, result[x], calendarId, true);

                                }

                            });
                        }

                    }).catch(function (error) {
                        console.error(">> error = ", error);
                    })

                    // save the user
                    newUser.save(function (err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });


    }));

    function getGoogleCalendarEvents(calendarId, token, cb) {
        var google_calendar = new gcal.GoogleCalendar(token);
        google_calendar.events.list(calendarId, function (err, data) {
            return new Promise(function (resolve, reject) {
                if (err) {
                    return reject(err);
                }
                return resolve(cb(data));
            });

        });
    }

    function saveGcaltoMongodb(gCalModel, result, calendarId, isUpdate) {
        gCalModel.summary = result.summary;
        gCalModel.datetime = result.start.dateTime;
        gCalModel.attendees = result.attendees;

        if (!isUpdate) {
            gCalModel.email = calendarId;
            gCalModel.gcalId = result.id;
        }

        gCalModel.save(function (err, result) {
            if (err)
                console.log(err);
            //console.log(result);
        });
    }

    function saveGclienttoMongodb(gClientModel, result, calendarId, isUpdate) {
        gClientModel.name = result.names[0].displayName
        gClientModel.mobile = result.phoneNumbers[0].value;
        gClientModel.clientemail = result.emailAddresses[0].value;
        console.log(gClientModel.name)
        console.log(gClientModel.mobile)
        console.log(gClientModel.clientemail)

        if (!isUpdate) {
            gClientModel.useremail = calendarId;
            gClientModel.name = result.names[0].displayName;
        }

        gClientModel.save(function (err, result) {
            if (err)
                console.log(err);
            //console.log(result);
        });
    }

    function getGooglePeople(calendarId, token, refreshToken) {

        const oauth2Client = new OAuth2(config.clientID, config.clientSecret, config.callbackURL);
        //Set the access token that we got
        oauth2Client.credentials = {
            access_token: token,
            refresh_token: refreshToken
        };

        const listConfig = {
            resourceName: 'people/me',
            "requestMask.includeField": "person.names,person.phoneNumbers,person.emailAddresses" //no spaces
        };
        const peopleAPI = googleapi.people({
            version: "v1",
            auth: oauth2Client
        });

        //console.log("People Here")
        return new Promise(function (resolve, reject) {
            peopleAPI.people.connections.list(listConfig,
                function (error, result) {
                    console.log("People Error = ", error)
                    if (error) {
                        return reject(error);
                    }
                    if (error === null) {
                        //console.log("No contacts exists " + error)
                        return reject(error);
                    }
                    //console.log(result)
                    return resolve(result.connections);
                });
        })
    }

    //PATCH calendar

    // clientID: config.Google_key,
    // clientSecret: config.Google_secret,
    // callbackURL: config.Google_callback_url

    app.patch("/api/gcalupdate/:gcalId", function (req, res) {

        console.log("TOKEN " + req.user.google.token)
        console.log("REFRESH TOKEN " + req.user.google.refreshToken)

        const oauth2Client = new OAuth2(config.Google_key, config.Google_secret, config.Google_callback_url);

        oauth2Client.credentials = {
            access_token: req.user.google.token,
            refresh_token: req.user.google.refreshToken
        };

        const calendarAPI = googleapi.calendar({
            version: "v3",
            auth: oauth2Client
        });


        console.log("gcalId " + req.params.gcalId);
        console.log("calendarId " + req.body.cal.email);

        calendarAPI.events.patch({
            'calendarId': req.body.cal.email,
            'eventId': req.params.gcalId,
            'resource': {
                'summary': req.body.cal.summary,
                'start': {
                    'dateTime': req.body.cal.datetime
                }
            }
        });

    });


    app.delete("/api/gcaldelete/:gcalId", function (req, res) {

        console.log("TOKEN " + req.user.google.token)
        console.log("REFRESH TOKEN " + req.user.google.refreshToken)

        const oauth2Client = new OAuth2(config.Google_key, config.Google_secret, config.Google_callback_url);

        oauth2Client.credentials = {
            access_token: req.user.google.token,
            refresh_token: req.user.google.refreshToken
        };

        const calendarAPI = googleapi.calendar({
            version: "v3",
            auth: oauth2Client
        });

        console.log("PARAMS " + JSON.stringify(req.params));
        console.log("BODY " + JSON.stringify(req.body));

        console.log("gcalId " + req.params.gcalId);
        console.log("calendarId " + req.user.google.email);

        calendarAPI.events.delete({
            'calendarId': req.user.google.email,
            'eventId': req.params.gcalId
        });

    });


    passport.serializeUser(function (user, done) {
        //console.log('passport.serializeUser: ' + username);
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        //console.log('passport.deserializeUser: ' + user.id);
        User.findById(id, function (err, user) {
            console.log('deserializing user:', user);
            done(err, user);
        });
    });

    var isAuthenticated = function (req, res, next) {
        //console.log("isAuthenticated(): ", req.user);
        if (req.isAuthenticated()) {
            next(); //good moves to the next one 
        } else {
            res.sendStatus(401);
        }
    }

    // var isValidPassword = function(user, password){
    //     return bCrypt.compareSync(password, this.local.password);
    // }

    var bcrypt = require('bcrypt-nodejs');

    var validPassword = function (user, password) {

        if (user !== null) {
            return bcrypt.compareSync(password, user.local.password);
        }
    };

    return {
        isAuthenticated: isAuthenticated,
    }


};